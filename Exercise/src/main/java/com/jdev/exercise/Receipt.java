package com.jdev.exercise;

import static java.math.BigDecimal.ZERO;

import java.io.PrintStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.jdev.exercise.pricer.DiscountPricerSession;

public class Receipt {
    private final static BigDecimal MINUS_ONE = new BigDecimal("-1.0");
    private List<String> lines = new ArrayList<>();

    public Receipt(CheckoutSession checkoutSession) {
	create(checkoutSession);
    }

    private void create(CheckoutSession checkoutSession) {
	BigDecimal subTotal = checkoutSession.getSubTotal();
	BigDecimal totalDiscount = checkoutSession.getTotalDiscount();

	append("========================================");
	append("               Receipt");
	append("========================================");

	for (SKU item : checkoutSession.getItems()) {
	    append(item.toString());
	}
	append("----------------------------------------");

	append(String.format("%-30s  %8.2f", "Sub Total:", subTotal));

	append("");

	if (!totalDiscount.equals(ZERO)) {
	    for (DiscountPricerSession discountPricerSession : checkoutSession.getDiscountPricerSessions()) {
		BigDecimal discountedPrice = discountPricerSession.getDiscountedTotal();
		if (!discountedPrice.equals(ZERO)) {
		    append(discountPricerSession.toString());
		}
	    }

	    append("----------------------------------------");
	    append(String.format("%-30s  %8.2f", "Total Discount", totalDiscount.multiply(MINUS_ONE)));
	}

	append("========================================");
	append(String.format("%-30s  %8.2f", "Total", subTotal.subtract(totalDiscount)));
	append("Items: " + checkoutSession.getItems().size());

	append("========================================");
    }

    public void append(String line) {
	lines.add(line);
    }

    public void output(PrintStream out) {
	out.println();
	for (String line : lines) {
	    out.println(line);
	}
	out.println();
    }

}

package com.jdev.exercise;

import java.math.BigDecimal;

import com.jdev.exercise.pricer.Pricer;

public interface SKU {
    public Integer getCode();

    public String getDescription();

    public void setDescription(String description);

    public void setUnitPrice(String unitPrice);

    public BigDecimal getUnitPrice();

    public void setPricer(Pricer pricer);

    public Pricer getPricer();
}

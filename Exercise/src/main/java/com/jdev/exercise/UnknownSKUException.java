package com.jdev.exercise;

public class UnknownSKUException extends Exception {

    private static final long serialVersionUID = 1L;

    public UnknownSKUException(String itemDescription) {
	super("Unknown SKU [" + itemDescription + "]");
    }

}

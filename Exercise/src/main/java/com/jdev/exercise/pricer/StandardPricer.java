package com.jdev.exercise.pricer;

import static java.math.BigDecimal.ZERO;

import java.math.BigDecimal;

import com.jdev.exercise.AbstractPricer;

public class StandardPricer extends AbstractPricer implements Pricer {
    private static final StandardPricer instance = new StandardPricer();

    private StandardPricer() {
	super(0, "");
    }

    @Override
    public boolean isValid() {
	return true;
    }

    public Integer getPurchaseMultiple() {
	return 1;
    }

    public Integer getChargingMultiple() {
	return 1;
    }

    public BigDecimal getDiscountedAmount(Integer itemCount, BigDecimal unitPrice) {
	return ZERO;
    }

    public static StandardPricer getInstance() {
	return instance;
    }

    @Override
    public PricerSession newPricerSession() {
	return new PricerSessionImpl(this);
    }

    private static class PricerSessionImpl extends AbstractPricerSession implements PricerSession {
	private final Pricer pricer;

	private PricerSessionImpl(Pricer pricer) {
	    this.pricer = pricer;
	}

	@Override
	public String toString() {
	    return pricer.getCode() + " " + pricer.getDescription() + " Total: " + String.format("%-8.2f", getTotal());
	}

	@Override
	public Integer getPricerCode() {
	    return pricer.getCode();
	}

    }

}

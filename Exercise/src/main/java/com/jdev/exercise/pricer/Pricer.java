package com.jdev.exercise.pricer;

public interface Pricer {
    public Integer getCode();

    public String getDescription();

    public boolean isValid();

    public PricerSession newPricerSession();
}

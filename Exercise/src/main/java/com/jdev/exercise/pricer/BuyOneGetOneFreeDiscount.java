package com.jdev.exercise.pricer;

public class BuyOneGetOneFreeDiscount extends MultiBuyUnitPricer {

    public BuyOneGetOneFreeDiscount(Integer code) throws Exception {
	super(code, "Buy One Get One Free", 2, 1);

    }

}

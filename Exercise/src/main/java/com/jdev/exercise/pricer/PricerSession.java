package com.jdev.exercise.pricer;

import java.math.BigDecimal;

import com.jdev.exercise.SKU;

public interface PricerSession {
    public Integer getPricerCode();

    public void add(SKU sku);

    public BigDecimal getTotal();
}

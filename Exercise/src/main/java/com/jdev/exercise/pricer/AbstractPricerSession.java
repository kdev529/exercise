package com.jdev.exercise.pricer;

import static java.math.BigDecimal.ZERO;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.jdev.exercise.SKU;

public abstract class AbstractPricerSession implements PricerSession {
    protected List<SKU> items = new ArrayList<>();

    protected AbstractPricerSession() {

    }

    @Override
    public void add(SKU sku) {
	items.add(sku);
    }

    public Collection<SKU> getItems() {
	return items;
    }

    @Override
    public BigDecimal getTotal() {
	BigDecimal total = ZERO;

	for (SKU sku : items) {
	    total = total.add(sku.getUnitPrice());
	}
	return total;
    }

}

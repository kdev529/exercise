package com.jdev.exercise;

import java.util.Collection;

public interface StockSystem {
    public SKU findByCode(Integer code) throws UnknownSKUException;

    public SKU findByDescription(String description) throws UnknownSKUException;

    public SKU defineItem(Integer code, String description, String unitPrice) throws UnknownSKUException;

    public Collection<SKU> getSKUList();

}

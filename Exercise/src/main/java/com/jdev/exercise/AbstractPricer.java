package com.jdev.exercise;

public abstract class AbstractPricer {
    private final Integer code;
    private final String description;

    protected AbstractPricer(Integer code, String description) {
	this.code = code;
	this.description = description;
    }

    public Integer getCode() {
	return code;
    }

    public String getDescription() {
	return description;
    }

}

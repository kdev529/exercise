package com.jdev.exercise;

import java.util.Collection;

public interface CheckoutSystem {

    public CheckoutSession checkout(Collection<String> itemNames);

}

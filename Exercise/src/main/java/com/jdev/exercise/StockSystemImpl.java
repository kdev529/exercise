package com.jdev.exercise;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

public class StockSystemImpl implements StockSystem {
    private static final List<SKU> skuList = new ArrayList<SKU>();

    @Override
    public SKU findByCode(Integer code) throws UnknownSKUException {
	Optional<SKU> match = skuList.stream().filter(s -> s.getCode().equals(code)).findFirst();
	if (match.isPresent()) {
	    return match.get();
	} else {
	    throw new UnknownSKUException(code.toString());
	}
    }

    @Override
    public SKU findByDescription(String description) throws UnknownSKUException {
	Optional<SKU> match = skuList.stream().filter(s -> s.getDescription().equalsIgnoreCase(description)).findFirst();
	if (match.isPresent()) {
	    return match.get();
	} else {
	    throw new UnknownSKUException(description);
	}
    }

    @Override
    public SKU defineItem(Integer code, String description, String unitPrice) {

	SKU sku = null;

	try {
	    sku = findByCode(code);
	} catch (UnknownSKUException e) {
	    // NOP
	}

	if (sku == null) {
	    sku = new Item(code);
	}

	sku.setDescription(description);
	sku.setUnitPrice(unitPrice);

	if (!skuList.contains(sku)) {
	    skuList.add(sku);
	}

	return sku;
    }

    @Override
    public Collection<SKU> getSKUList() {
	return skuList;
    }

}

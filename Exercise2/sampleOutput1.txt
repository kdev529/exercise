Picked up _JAVA_OPTIONS: -Dawt.useSystemAAFontSettings=on -Dswing.aatext=true
========================================
Store Catalogue
Apple                               0.60
Orange                              0.25
Banana                              0.20
========================================

Input: [Apple, APple, orange, ApplE]
Parse: [Apple, APple, orange, ApplE]

Checkout
Apple                               0.60
Apple                               0.60
Orange                              0.25
Apple                               0.60

SubTotal: 2.05    
Discount: 0.60    
Total   : 1.45    

========================================
               Receipt
========================================
Apple                               0.60
Apple                               0.60
Orange                              0.25
Apple                               0.60
----------------------------------------
Sub Total:                          2.05

#123  3 for 2 Cheapest Free
 Apple -    1 @ 0.60              -0.60

----------------------------------------
Total Discount                     -0.60
========================================
Total                               1.45
Items: 4
========================================

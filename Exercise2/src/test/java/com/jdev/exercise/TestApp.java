package com.jdev.exercise;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;

import com.jdev.exercise.pricer.DiscountPricerSession;
import com.jdev.exercise.pricer.MultiBuyUnitPricer;
import com.jdev.exercise.pricer.ThreeForTwoDiscount;

import junit.framework.TestCase;

public class TestApp extends TestCase {

    private StockSystem stockSystem;

    private CheckoutSystem checkoutSystem;

    @Override
    @Before
    public void setUp() throws Exception {
	super.setUp();

	stockSystem = new StockSystemImpl();
	checkoutSystem = new CheckoutImpl(stockSystem);

	stockSystem.defineItem(1, "Apple", "0.40");
	stockSystem.defineItem(2, "Orange", "0.25");
	stockSystem.defineItem(3, "Pear", "0.50");
	stockSystem.defineItem(4, "Banana", "0.30");

    }

    @Test
    public void testFindByDescription() throws Exception {

	String[] names = { "Orange", "Apple", "Pear" };

	assertEquals(stockSystem.getSKUList().size(), 4);

	for (String name : names) {
	    SKU skuFromName = stockSystem.findByDescription(name);
	    SKU skuFromUppercaseName = stockSystem.findByDescription(name.toUpperCase());
	    SKU skuFromLowercaseName = stockSystem.findByDescription(name.toLowerCase());

	    assertEquals("Find By Name " + name, skuFromName.getDescription(), name);
	    assertEquals("Find By Uppercase Name " + name, skuFromUppercaseName.getDescription(), name);
	    assertEquals("Find By Lowercase Name " + name, skuFromLowercaseName.getDescription(), name);

	    assertSame(skuFromName, skuFromUppercaseName);
	    assertSame(skuFromName, skuFromLowercaseName);

	}
    }

    @Test
    public void testUnitPrice() throws Exception {

	SKU apple = stockSystem.findByDescription("Apple");
	SKU pear = stockSystem.findByDescription("peaR");
	SKU orange = stockSystem.findByDescription("oRange");

	assertEquals("UnitPrice of Apple", apple.getUnitPrice(), new BigDecimal("0.40"));
	assertEquals("UnitPrice of Orange", orange.getUnitPrice(), new BigDecimal("0.25"));
	assertEquals("UnitPrice of Pear", pear.getUnitPrice(), new BigDecimal("0.50"));

    }

    @Test
    public void testBuyOneGetOneFree() throws Exception {
	assertEquals(stockSystem.getSKUList().size(), 4);

	stockSystem.findByDescription("Apple").setPricer(new MultiBuyUnitPricer(999, "Buy One Get One Free", 2, 1));

	CheckoutSession checkoutSession = checkoutSystem.checkout(Arrays.asList(
		new String[] { "Apple", "Orange", "Apple", "Orange", "Apple", "Orange", "Apple", "orange", "Apple", "Orange", "APple", "apple" }));

	assertEquals(checkoutSession.getItems().size(), 12);

	DiscountPricerSession pricerSession = null;

	Optional<DiscountPricerSession> match = checkoutSession.getDiscountPricerSessions().stream().filter(s -> s.getPricerCode() == 999)
		.findFirst();
	if (match.isPresent()) {
	    pricerSession = match.get();
	}

	assertNotNull(pricerSession);

	assertEquals("Two For One Total", pricerSession.getTotal(), new BigDecimal("2.80"));
	assertEquals("Two For One Discount", pricerSession.getDiscountSum(), new BigDecimal("1.20"));
    }

    @Test
    public void testThreeForTwo() throws Exception {
	assertEquals(stockSystem.getSKUList().size(), 4);

	stockSystem.findByDescription("Orange").setPricer(new ThreeForTwoDiscount(9342));

	CheckoutSession checkoutSession = checkoutSystem.checkout(Arrays.asList(
		new String[] { "Apple", "Orange", "Apple", "Orange", "Apple", "Orange", "Apple", "orange", "Apple", "Orange", "APple", "apple" }));

	assertEquals(checkoutSession.getItems().size(), 12);

	DiscountPricerSession pricerSession = null;

	Optional<DiscountPricerSession> match = checkoutSession.getDiscountPricerSessions().stream().filter(s -> s.getPricerCode() == 9342)
		.findFirst();
	if (match.isPresent()) {
	    pricerSession = match.get();
	}

	assertNotNull(pricerSession);

	assertEquals("Two For One Total", pricerSession.getTotal(), new BigDecimal("1.25"));
	assertEquals("Two For One Discount", pricerSession.getDiscountSum(), new BigDecimal("0.25"));
    }

}

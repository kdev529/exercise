package com.jdev.exercise;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;

import com.jdev.exercise.pricer.DiscountPricerSession;
import com.jdev.exercise.pricer.Pricer;
import com.jdev.exercise.pricer.ThreeForTwoCheapestFree;

import junit.framework.TestCase;
import static java.lang.System.out;
public class TestThreeForTwoCheapestFree extends TestCase {

    private static final int PRICER_CODE=999;
    private StockSystem stockSystem;
    private CheckoutSystem checkoutSystem;
    private Pricer pricer=null;

    @Override
    @Before
    public void setUp() throws Exception {
	super.setUp();

	stockSystem = new StockSystemImpl();
	checkoutSystem = new CheckoutImpl(stockSystem);

	stockSystem.defineItem(1, "Apple", "0.40");
	stockSystem.defineItem(2, "Orange", "0.25");
	stockSystem.defineItem(3, "Pear", "0.50");
	stockSystem.defineItem(4, "Banana", "0.30");

	assertEquals(stockSystem.getSKUList().size(), 4);
	
	pricer=new ThreeForTwoCheapestFree(PRICER_CODE);
	stockSystem.findByDescription("Apple").setPricer(pricer);
	stockSystem.findByDescription("Banana").setPricer(pricer);
	pricer.setDebug(true);
	
    }

    @Test
    public void testThreeForTwoCheapestFree1() throws Exception {

	callTest(new BigDecimal("1.20"), new BigDecimal("0.40"), new String[] { "Apple", "Apple" ,"Apple"});
    }

    @Test
    public void testThreeForTwoCheapestFree2() throws Exception {

	callTest(new BigDecimal("1.50"), new BigDecimal("0.30"), new String[] { "Apple", "Apple" ,"Apple", "Banana"});
    }
    
    public void testThreeForTwoCheapestFree3() throws Exception {

	callTest(new BigDecimal("2.10"), new BigDecimal("0.60"), new String[] { "Apple", "Apple" ,"Apple", "Banana", "Banana", "banana"});
    }
    
    
    private DiscountPricerSession findPricerSession(CheckoutSession checkoutSession, int pricerCode) 
    {
	Optional<DiscountPricerSession> match = checkoutSession.getDiscountPricerSessions().stream().filter(s -> s.getPricerCode() == pricerCode).findFirst();
	return match.isPresent() ? match.get() : null;
    }


    private void callTest(BigDecimal expectedTotal, BigDecimal expectedDiscountSum, String...itemList) throws Exception
    {
	CheckoutSession checkoutSession = checkoutSystem.checkout(itemList);
		
	DiscountPricerSession pricerSession = findPricerSession(checkoutSession, PRICER_CODE);
	assertNotNull(pricerSession);

	 BigDecimal discountSum=pricerSession.getDiscountSum();
	 BigDecimal total=pricerSession.getTotal();

	 out.println();
	 out.println(String.format("SubTotal  %-8.2f", total));
	 out.println(String.format("Discount  %-8.2f", discountSum));
	 out.println(String.format("Total     %-8.2f", total.subtract(discountSum)));
	
	 assertEquals(pricer.getDescription()+ " SubTotal", total, expectedTotal);
 	 assertEquals(pricer.getDescription() +" DiscountedTotal", discountSum, expectedDiscountSum);
	
    }
    
}

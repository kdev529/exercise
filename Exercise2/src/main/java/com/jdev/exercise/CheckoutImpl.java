package com.jdev.exercise;

import static java.lang.System.out;

import java.util.Arrays;
import java.util.Collection;

public class CheckoutImpl implements CheckoutSystem {
    private final StockSystem stockSystem;

    public CheckoutImpl(StockSystem stockSystem) {
	this.stockSystem = stockSystem;
    }

    @Override
    public CheckoutSession checkout(String[] itemNames) {
	return checkout(Arrays.asList(itemNames));
    }

    @Override
    public CheckoutSession checkout(Collection<String> itemNames) {

	out.println();
	out.println("Checkout");

	CheckoutSession checkoutSession = new CheckoutSession();

	for (String itemName : itemNames) {
	    try {

		SKU sku = stockSystem.findByDescription(itemName);
		checkoutSession.add(sku);
		out.println(sku);

	    } catch (Exception e) {
		out.println(e.getMessage());
	    }

	}

	out.println();
	return checkoutSession;
    }

}

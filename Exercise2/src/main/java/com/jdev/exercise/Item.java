package com.jdev.exercise;

import static java.math.BigDecimal.ROUND_HALF_UP;
import static java.math.BigDecimal.ZERO;

import java.math.BigDecimal;

import com.jdev.exercise.pricer.Pricer;
import com.jdev.exercise.pricer.StandardPricer;

public class Item implements SKU {
    private final Integer code; // strictly not required for this exercise, but usefull nonetheless for tracing
				// and testing
    private String description = "";
    private BigDecimal unitPrice = ZERO;
    private Pricer pricer = null;

    public Item(Integer code) {
	this.code = code;
    }

    public Item(Integer code, String description) {
	this.code = code;
	setDescription(description);
    }

    @Override
    public BigDecimal getUnitPrice() {
	return unitPrice;
    }

    public void setUnitPrice(BigDecimal value) {
	value.setScale(2, ROUND_HALF_UP);
	this.unitPrice = value;
    }

    @Override
    public void setUnitPrice(String value) {
	setUnitPrice(new BigDecimal(value));
    }

    @Override
    public Integer getCode() {
	return code;
    }

    @Override
    public String getDescription() {
	return description;
    }

    @Override
    public void setDescription(String description) {
	this.description = description.trim();
    }

    @Override
    public void setPricer(Pricer pricer) {
	this.pricer = pricer;
    }

    @Override
    public Pricer getPricer() {
	return pricer != null ? pricer : StandardPricer.getInstance();
    }

    @Override
    public String toString() {
	return (String.format("%-30s  %8.2f", getDescription(), getUnitPrice()));
    }

}

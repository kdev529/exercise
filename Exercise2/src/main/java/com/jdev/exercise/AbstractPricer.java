package com.jdev.exercise;

public abstract class AbstractPricer {
    private final Integer code;
    private final String description;
    protected boolean debug = false;// in absence of log4J or something...

    protected AbstractPricer(Integer code, String description) {
	this.code = code;
	this.description = description;
    }

    public Integer getCode() {
	return code;
    }

    public String getDescription() {
	return description;
    }

    public boolean isDebug() {
	return debug;
    }

    public void setDebug(boolean debug) {
	this.debug = debug;
    }

}

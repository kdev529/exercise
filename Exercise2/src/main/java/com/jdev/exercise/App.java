package com.jdev.exercise;

import static java.lang.System.out;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.jdev.exercise.pricer.MultiBuyUnitPricer;
import com.jdev.exercise.pricer.ThreeForTwoCheapestFree;

public class App {
    private final StockSystem stockSystem = new StockSystemImpl();

    private final CheckoutSystem checkoutSystem = new CheckoutImpl(stockSystem);

    public static void main(String[] args) throws Exception {
	if (args.length > 0) {
	    App instance = new App();
	    instance.run(args);
	} else {
	    out.println("Supply list of items on command line");
	}

    }

    public App() throws Exception {
	setup();
    }

    private void setup() throws Exception {

	SKU apple = stockSystem.defineItem(1, "Apple", "0.6");
	SKU orange = stockSystem.defineItem(2, "Orange", "0.25");
	SKU banana = stockSystem.defineItem(3, "Banana", "0.20");

	// comment following lines to omit discounts

	ThreeForTwoCheapestFree pricer = new ThreeForTwoCheapestFree(123);
	apple.setPricer(pricer);
	banana.setPricer(pricer);
	// apple.setPricer(new MultiBuyUnitPricer(241, "Buy One Get One Free", 2, 1));

	orange.setPricer(new MultiBuyUnitPricer(342, "3 for the price of 2", 3, 2));

	// alternatively, use these predefined ones... - same as above, but more
	// convenient
	// apple.setPricer(new BuyOneGetOneFreeDiscount(9241));
	// orange.setPricer(new ThreeForTwoDiscount(9342));

	out.println("========================================");
	out.println("Store Catalogue");
	for (SKU sku : stockSystem.getSKUList()) {
	    out.println(sku);
	}
	out.println("========================================");
	out.println();

    }

    public void run(String[] nameList) {
	List<String> itemNames = new ArrayList<>();

	out.println("Input: " + Arrays.asList(nameList).toString());

	for (String name : nameList) {
	    String value = name.trim();
	    if (!value.isEmpty()) {
		itemNames.add(name);
	    }

	}

	if (!itemNames.isEmpty()) {
	    out.println("Parse: " + itemNames.toString());
	    CheckoutSession checkoutSession = checkoutSystem.checkout(itemNames);

	    BigDecimal subTotal = checkoutSession.getSubTotal();
	    BigDecimal discount = checkoutSession.getTotalDiscount();

	    out.println(String.format("SubTotal: %-8.2f", subTotal));
	    out.println(String.format("Discount: %-8.2f", discount));
	    out.println(String.format("Total   : %-8.2f", subTotal.subtract(discount)));

	    new Receipt(checkoutSession).output(out);

	} else {
	    out.println("Input values not parsed successfully");
	}

    }

}

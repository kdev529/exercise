package com.jdev.exercise;

import static java.math.BigDecimal.ZERO;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jdev.exercise.pricer.DiscountPricerSession;
import com.jdev.exercise.pricer.Pricer;
import com.jdev.exercise.pricer.PricerSession;

public class CheckoutSession {
    private List<SKU> items = new ArrayList<>();
    private Map<Pricer, PricerSession> pricerSessionMap = new HashMap<>();
    private List<DiscountPricerSession> discountPricerSessions = new ArrayList<>();

    /*
     * add a single item at the checkout
     */
    public void add(SKU sku) throws Exception {
	getPricerSession(sku.getPricer()).add(sku);
	items.add(sku);
    }

    private PricerSession getPricerSession(Pricer pricer) {
	PricerSession pricerSession = pricerSessionMap.get(pricer);

	if (pricerSession == null) {
	    pricerSession = pricer.newPricerSession();
	    pricerSessionMap.put(pricer, pricerSession);

	    if (DiscountPricerSession.class.isInstance(pricerSession)) {
		discountPricerSessions.add((DiscountPricerSession) pricerSession);
	    }
	}

	return pricerSession;
    }

    public Collection<DiscountPricerSession> getDiscountPricerSessions() {
	return discountPricerSessions;
    }

    public Collection<PricerSession> getPricerSessions() {
	return pricerSessionMap.values();
    }

    public Collection<SKU> getItems() {
	return items;
    }

    public BigDecimal getSubTotal() {
	BigDecimal subTotal = ZERO;

	for (PricerSession pricerSession : getPricerSessions()) {
	    subTotal = subTotal.add(pricerSession.getTotal());
	}

	// Java 8...return getPricerSessions().stream().map((x) ->
	// x.getTotal()).reduce((x, y) ->x.add(y)).get();

	return subTotal;
    }

    public BigDecimal getTotalDiscount() {
	BigDecimal total = ZERO;

	for (DiscountPricerSession discountPricerSession : getDiscountPricerSessions()) {
	    total = total.add(discountPricerSession.getDiscountSum());
	}

	return total;
    }

}

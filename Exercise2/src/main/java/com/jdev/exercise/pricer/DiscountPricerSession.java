package com.jdev.exercise.pricer;

import java.math.BigDecimal;

public interface DiscountPricerSession extends PricerSession {
    public BigDecimal getDiscountSum();
}

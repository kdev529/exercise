package com.jdev.exercise.pricer;

public class ThreeForTwoCheapestFree extends MultiBuyUnitPricer2 {

    public ThreeForTwoCheapestFree(int code, String description) throws Exception {
	super(code, description, 3, 2, true);
    }

    public ThreeForTwoCheapestFree(int code) throws Exception {
	super(code, "3 for 2 Cheapest Free", 3, 2, true);
    }

}

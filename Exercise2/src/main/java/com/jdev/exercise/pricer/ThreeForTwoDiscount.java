package com.jdev.exercise.pricer;

public class ThreeForTwoDiscount extends MultiBuyUnitPricer {

    public ThreeForTwoDiscount(Integer code) throws Exception {
	super(code, "3 for the price of 2", 3, 2);

    }

}

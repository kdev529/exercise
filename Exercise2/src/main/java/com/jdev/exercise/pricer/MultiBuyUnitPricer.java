package com.jdev.exercise.pricer;

import static java.math.BigDecimal.ZERO;

import java.math.BigDecimal;

import com.jdev.exercise.AbstractPricer;
import com.jdev.exercise.SKU;

/**
 * 
 * @deprecated inherit from MultiBuyUnitPricer2, once it has been fully tested
 */
public class MultiBuyUnitPricer extends AbstractPricer implements Pricer, Discount {
    private final Integer purchaseMultiple;
    private final Integer chargingMultiple;

    public MultiBuyUnitPricer(Integer code, String description, Integer purchaseMultiple, Integer chargingMultiple) throws Exception {
	super(code, description);
	this.purchaseMultiple = purchaseMultiple;
	this.chargingMultiple = chargingMultiple;

	if (!isValid()) {
	    throw new Exception("INVALID settings for Pricer [" + getCode() + " " + getDescription());
	}
    }

    @Override
    public boolean isValid() {
	return purchaseMultiple > 1 && chargingMultiple > 0 && purchaseMultiple > chargingMultiple;
    }

    public Integer getPurchaseMultiple() {
	return purchaseMultiple;
    }

    public Integer getChargingMultiple() {
	return chargingMultiple;
    }

    private BigDecimal getDiscountedAmount(Integer itemCount, BigDecimal unitPrice) {

	if (isValid() && itemCount >= purchaseMultiple) {
	    int quotient = itemCount / purchaseMultiple;
	    int remainder = itemCount % purchaseMultiple;

	    BigDecimal offerPrice = unitPrice.multiply(new BigDecimal(quotient * chargingMultiple));
	    if (remainder > 0) {
		offerPrice = offerPrice.add(unitPrice.multiply(new BigDecimal(remainder)));
	    }
	    BigDecimal total = unitPrice.multiply(new BigDecimal(itemCount));

	    return total.subtract(offerPrice);
	} else {
	    return ZERO;
	}

    }

    /**
     * 
     * TODO FIXME - The aspiration is to make this support discounts over mixed
     * SKUs. The current implementation uses the unit price of the first element in
     * the item list this should not matter right now as due the current design
     * where the limitation/caveat is SKUs will not share an instance of a Pricer -
     * see getDiscountedTotal.
     */
    private static class PricerSessionImpl extends AbstractPricerSession implements DiscountPricerSession {
	private final static BigDecimal MINUS_ONE = new BigDecimal("-1.0");
	private final MultiBuyUnitPricer pricer;

	private PricerSessionImpl(MultiBuyUnitPricer pricer) {
	    this.pricer = pricer;
	}

	@Override
	public BigDecimal getDiscountSum() {
	    if (!items.isEmpty()) {
		// TODO FIXME - see comment at class level. caveat regarding unitPrice
		return pricer.getDiscountedAmount(items.size(), items.get(0).getUnitPrice());
	    } else {
		return ZERO;
	    }

	}

	@Override
	public String toString() {
	    StringBuilder sb = new StringBuilder();

	    if (!items.isEmpty()) {
		SKU sku = items.get(0);

		sb.append(String.format("#%-4d %s", pricer.getCode(), pricer.getDescription()));
		sb.append(System.lineSeparator());
		BigDecimal discountedSum = sku.getUnitPrice().multiply(new BigDecimal(items.size())).multiply(MINUS_ONE);

		sb.append(String.format(" %s - %4d @ %-8.2f       %8.2f", sku.getDescription(), items.size(), sku.getUnitPrice(), discountedSum));
		sb.append(System.lineSeparator());
	    }

	    return sb.toString();
	}

	@Override
	public Integer getPricerCode() {
	    return pricer.getCode();
	}

    }

    @Override
    public PricerSession newPricerSession() {
	return new PricerSessionImpl(this);
    }

}

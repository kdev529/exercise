package com.jdev.exercise.pricer;

import static java.lang.System.out;
import static java.math.BigDecimal.ZERO;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.jdev.exercise.AbstractPricer;
import com.jdev.exercise.SKU;

/**
 * MultiBuy Discount Pricer with cheapest item free. Note: Limited testing
 * across only 2 SKUs
 */
public abstract class MultiBuyUnitPricer2 extends AbstractPricer implements Pricer, Discount {
    private final Integer purchaseMultiple;
    private final Integer chargingMultiple;
    private final boolean cheapestItemFree;

    public MultiBuyUnitPricer2(Integer code, String description, Integer purchaseMultiple, Integer chargingMultiple, boolean cheapestItemFree)
	    throws Exception {
	super(code, description);
	this.purchaseMultiple = purchaseMultiple;
	this.chargingMultiple = chargingMultiple;
	this.cheapestItemFree = cheapestItemFree;

	if (!isValid()) {
	    throw new Exception("INVALID settings for Pricer [" + getCode() + " " + getDescription());
	}
    }

    @Override
    public boolean isValid() {
	return purchaseMultiple > 1 && chargingMultiple > 0 && purchaseMultiple > chargingMultiple;
    }

    public Integer getPurchaseMultiple() {
	return purchaseMultiple;
    }

    public Integer getChargingMultiple() {
	return chargingMultiple;
    }

    public boolean isCheapestItemFree() {
	return cheapestItemFree;
    }

    /**
     * Return the elements, sorted on UnitPrice and Name
     * 
     * @param items
     * @return
     */
    private List<SKU> skuOrderedOnAscendingPriceAndName(Collection<SKU> items) {

	List<SKU> list = new ArrayList<SKU>(items);
	list.sort(new Comparator<SKU>() {
	    public int compare(SKU a, SKU b) {
		int comparison = a.getUnitPrice().compareTo(b.getUnitPrice());
		if (comparison != 0) {
		    return comparison;
		} else {
		    return a.getDescription().compareToIgnoreCase(b.getDescription());
		}
	    }
	});
	return list;
    }

    /**
     * Return the elements, sorted on Name
     * 
     * @param items
     * @return
     */
    private List<SKU> skuOrderedOnAscendingName(Collection<SKU> items) {

	List<SKU> list = new ArrayList<SKU>(items);
	list.sort(new Comparator<SKU>() {
	    public int compare(SKU a, SKU b) {
		return a.getDescription().compareToIgnoreCase(b.getDescription());
	    }
	});
	return list;
    }

    private Map<SKU, Allocation> allocate(List<SKU> items) {
	Map<SKU, Allocation> allocations = new LinkedHashMap<>();
	Collection<SKU> typesOfSkuAtCheckoutForThisDiscountScheme = skuOrderedOnAscendingName(new HashSet<>(items));

	for (SKU sku : typesOfSkuAtCheckoutForThisDiscountScheme) {
	    allocations.put(sku, new Allocation(sku, Collections.frequency(items, sku)));
	}

	LinkedList<SKU> list = new LinkedList<>(skuOrderedOnAscendingPriceAndName(items));
	if(isDebug())
	{
	for (SKU sku : list) {
	    out.println(sku.getDescription());
	}
	}

	int chagingMultipleInIteration;
	SKU skuToAllocate;
	for (int o = 0; o < (items.size() / purchaseMultiple) + 1; o++) {
	    for (chagingMultipleInIteration = 0; chagingMultipleInIteration < chargingMultiple; chagingMultipleInIteration++) {
		if (!list.isEmpty()) {
		    skuToAllocate = list.removeLast();
		    allocations.get(skuToAllocate).incrementFullPriceCount();
		}
	    }

	    if (!list.isEmpty()) {
		skuToAllocate = list.removeFirst();
		if (chagingMultipleInIteration == chargingMultiple) {
		    allocations.get(skuToAllocate).incrementFreeOfChargeCount();
		} else {
		    allocations.get(skuToAllocate).incrementFullPriceCount();
		}
	    }

	}

	return allocations;
    }

    private static class MultiBuyUnitPricerSession extends AbstractPricerSession implements DiscountPricerSession {
	private final static BigDecimal MINUS_ONE = new BigDecimal("-1.0");
	private final MultiBuyUnitPricer2 pricer;

	private MultiBuyUnitPricerSession(MultiBuyUnitPricer2 pricer) {
	    this.pricer = pricer;
	}

	private Map<SKU, Allocation> getAllocations() {
	    return pricer.allocate(items);
	}

	@Override
	public BigDecimal getDiscountSum() {
	    Map<SKU, Allocation> allocations = getAllocations();

	    BigDecimal freeOfChargeSum = ZERO;

	    if (isDebug()) {
		out.println();
	    }

	    for (Allocation allocation : allocations.values()) {
		if (isDebug()) {
		    out.println(allocation);
		}
		freeOfChargeSum = freeOfChargeSum.add(allocation.getFreeOfChargeSum());
	    }

	    return freeOfChargeSum;
	}

	private boolean isDebug() {
	    return pricer.isDebug();
	}

	@Override
	public String toString() {
	    StringBuilder sb = new StringBuilder();

	    sb.append(String.format("#%-4d %s", pricer.getCode(), pricer.getDescription()));
	    sb.append(System.lineSeparator());

	    for (Allocation allocation : getAllocations().values()) {
		if (allocation.getFreeItemCount() > 0) {
		    BigDecimal discountedSum = allocation.getFreeOfChargeSum().multiply(MINUS_ONE);

		    sb.append(String.format(" %s - %4d @ %-8.2f       %8.2f", allocation.getSku().getDescription(), allocation.getFreeItemCount(),
			    allocation.getSku().getUnitPrice(), discountedSum));
		    sb.append(System.lineSeparator());
		}
	    }

	    return sb.toString();
	}

	@Override
	public Integer getPricerCode() {
	    return pricer.getCode();
	}

    }

    @Override
    public PricerSession newPricerSession() {
	return new MultiBuyUnitPricerSession(this);
    }

    protected static class Allocation {
	private final SKU sku;
	private final int itemCount;
	private int fullPriceItemCount = 0;
	private int freeOfChargeItemCount = 0;

	protected Allocation(SKU sku, int total) {
	    this.sku = sku;
	    this.itemCount = total;
	}

	public BigDecimal getFreeOfChargeSum() {
	    return sku.getUnitPrice().multiply(new BigDecimal(freeOfChargeItemCount));
	}

	public BigDecimal getFullPriceSum() {
	    return sku.getUnitPrice().multiply(new BigDecimal(fullPriceItemCount));
	}

	public void incrementFreeOfChargeCount() {
	    freeOfChargeItemCount++;
	}

	public void incrementFullPriceCount() {
	    fullPriceItemCount++;
	}

	public int getFullPriceItemCount() {
	    return fullPriceItemCount;
	}

	public int getFreeItemCount() {
	    return freeOfChargeItemCount;
	}

	public int getItemCount() {
	    return itemCount;
	}

	public String toString() {
	    return String.format("%-10s %3d %3d %3d  %8.2f  %8.2f", sku.getDescription(), getItemCount(), getFullPriceItemCount(), getFreeItemCount(),
		    getFullPriceSum(), getFreeOfChargeSum());
	}

	public SKU getSku() {
	    return sku;
	}

    }

}
